/*
 * grunt-concat-dependencies
 * https://github.com/nelsonnogueira/concat-dependencies
 *
 * Copyright (c) 2014 nnogueira@etecture.co.uk
 * Licensed under the MIT license.
 */

'use strict';

var nodePath = require('path');

module.exports = function(grunt) {

    grunt.registerMultiTask('concat_dependencies', 'Resolves script dependencies and concatenates files.', function() {

        // Returns the .js file path for a given .ts or .ts.d
        var getJsPath = function (path) {
            return path.replace('.ts', '.js').replace('.d.', '.');
        }

        // Processes a dependency
        var processDependency = function (path) {
            grunt.log.writeln('Processing "' + path + '"...');
            filesVisited.push(path);

            // Get file contents
            var contents = grunt.file.read(path);

            // Define the dependency pattern and get any matches
            var regex = /\/\/\/ ?<reference path="([\.\/]*[A-Za-z0-9\/\.\-]+)(\.d)?"\/>/g;
            var matches = regex.exec(contents);

            // Process dependencies
            while (matches !== null) {
                var dependency = nodePath.normalize(path.substr(0, path.lastIndexOf('/')) + '/' + matches[1] + '.ts');
                var jsDependency = getJsPath(dependency);

                if (filesVisited.indexOf(dependency) === -1) {
                    processDependency(dependency);
                }

                if (dependencies.indexOf(jsDependency) === -1) {    // New dependency                    
                    dependencies.push(jsDependency);
                }

                matches = regex.exec(contents);
            }
        }

        // Merge task-specific and/or target-specific options with these defaults.
        var options = this.options({
            punctuation: ';',
            production: false
        });

        // Compute dependencies and store them to an Array
        var dependencies = [];
        var filesVisited = [];        
        var entryPoint = nodePath.normalize(options.entryPoint);

        // Process entry point, which will recursively fetch all other dependencies        
        processDependency(entryPoint);

        // Reverse the Array to ensure dependencies are loaded in the correct order <-- this isnt the correct logic
        // dependencies.reverse();
        dependencies.push(getJsPath(entryPoint));

        var outPath = this.files[0].dest;
        var outContent = '';
        var scriptTags = '<!-- Auto generated file. Do not edit! -->\n';
        var time = new Date().getTime();

        if (options.production) {
            scriptTags += '<script type="text/javascript" src="' + outPath + '"></script>';
        } else {
            // Concatenate dependencies
            for (var i = 0, length = dependencies.length; i < length; i++) {
                var dependency = dependencies[i];

                if(grunt.file.exists)
                    outContent += grunt.file.read(dependency) + options.punctuation;

                if (options.htmlTagOutput) {
                    var jsDependency = dependency.replace('assets/', '/static/');

                    if(options.cacheBust){
                        jsDependency+= '?t=' + time;
                    }

                    scriptTags += '<script type="text/javascript" src="' + jsDependency + '"></script>\n';
                }
            }
        }

        // Write the destination file(s)
        grunt.file.write(outPath, outContent);
        grunt.log.writeln('File "' + outPath + '" created.');

        if (options.htmlTagOutput) {
            grunt.file.write(options.htmlTagOutput, scriptTags);
            grunt.log.writeln('File "' + options.htmlTagOutput + '" created.');
        }

    });

};